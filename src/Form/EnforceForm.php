<?php

declare(strict_types=1);

namespace Drupal\config_enforce\Form;

use Drupal\config_enforce\Form\AbstractEnforceForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form for the Config Enforce module.
 */
class EnforceForm extends AbstractEnforceForm {

  const FORM_ID = 'config_enforce';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return $this
      ->setSharedFormProperties($form, $form_state)
      ->addEnforcementWarning()
      ->addConfigFormInfo()
      ->form();
  }

  /**
   * Warn users of possible data loss.
   */
  protected function addEnforcementWarning() {
    $message = 'Configuration from this form is being enforced. Any changes may be lost.';
    $tokens = [];
    $this->addWarning($message, $tokens);
    return $this;
  }

}
