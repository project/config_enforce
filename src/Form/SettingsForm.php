<?php

declare(strict_types=1);

namespace Drupal\config_enforce\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce\Form\FormHelperTrait;

/**
 * Defines a settings form for the Config Enforce Devel module.
 */
class SettingsForm extends ConfigFormBase {

  use FormHelperTrait;

  const FORM_ID = 'config_enforce_settings';

  const DEFAULT_TRIGGERS = [
    'cache_rebuild' => 'cache_rebuild',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_enforce.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $saved_config = $this->config('config_enforce.settings')->get('triggers');

    $this->form()['triggers'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'cache_rebuild' => 'Cache rebuild',
      ],
      '#title' => 'Trigger enforcement on:',
      '#description' => "Select events that will trigger a config enforcement.",
      '#default_value' => is_array($saved_config) ? $saved_config : self::DEFAULT_TRIGGERS,
    ];

    return parent::buildForm($this->form(), $this->formState());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $this->config('config_enforce.settings')
      ->set('triggers', $this->formState()->getValue('triggers'))
      ->save();

    return parent::submitForm($this->form(), $this->formState());
  }

}
