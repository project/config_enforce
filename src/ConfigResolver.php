<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;

/**
 * Helper class to extract information from config forms.
 */
class ConfigResolver {

  /* The form object from whence to extract configuration information. */
  protected $formObject;

  /* The form state from whence to extract the form object. */
  protected $formState;

  /* The form ID. */
  protected $formId;

  /**
   * A simple constructor to set the form state and object.
   */
  public function __construct(FormStateInterface $form_state) {
    $this->formState = $form_state;
    $this->formObject = $this->determineFormObject();
    $this->formId = $this->determineFormId();
  }

  /**
   * Return an array of config names associated with the form object.
   */
  public function getConfigNames() {
    $config_names = [];
    if ($this->hasSimpleConfig()) {
      $config_names = array_merge($config_names, $this->getSimpleConfigNames());
    }
    if ($this->hasConfigEntity()) {
      $config_names[] = $this->getConfigEntityName();
    }
    if ($this->hasConfigEntityList()) {
      $config_names = array_merge($config_names, $this->getConfigEntityListNames());
    }
    if ($this->hasPlugins()) {
      $config_names = array_merge($config_names, $this->getPluginConfigEntityNames());
    }
    return $config_names;
  }

  /**
   * Return the form ID.
   */
  public function getFormId() {
    return $this->formId;
  }

  /**
   * Determine whether the form is enforceable.
   */
  public function isAnEnforceableForm() {
    if (!$this->isAConfigForm()) return FALSE;
    return !in_array($this->getFormId(), $this->getFormDenylist());
  }

  /**
   * Determine whether the host form contains a config entity.
   */
  protected function hasConfigEntity() {
    // If there's no associated entity, it cannot be a config entity.
    if (!method_exists($this->formObject, 'getEntity')) return FALSE;

    $entity = $this->formObject->getEntity();

    // Skip entity creation, since the config object's name hasn't been determined yet.
    if ($entity->isNew()) return FALSE;

    // Skip content entities.
    if (method_exists($entity, 'bundleFieldDefinitions')) return FALSE;

    // We need to be able to determine the config entity's machine name.
    return method_exists($entity, 'getConfigDependencyName');
  }

  /**
   * Determine whether the host form is an instance of ConfigEntityListBuilder.
   */
  protected function hasConfigEntityList() {
    return ($this->formObject instanceof ConfigEntityListBuilder);
  }

  /**
   * Determine whether the host form contains any plugins.
   */
  protected function hasPlugins() {
    return (bool) count($this->getPluginConfigEntityNames());
  }

  /**
   * Determine whether the host form is a config form.
   */
  protected function isAConfigForm() {
    return (bool) count($this->getConfigNames());
  }

  /**
   * Return a list of forms to skip.
   */
  protected function getFormDenylist() {
    return \Drupal::moduleHandler()->invokeAll('config_enforce_form_denylist');
  }

  /**
   * Return the config name from a config entity form.
   */
  protected function getConfigEntityName() {
    return $this->formObject->getEntity()->getConfigDependencyName();
  }

  /**
   * Return the config name from a plugin config entity form.
   */
  protected function getPluginConfigEntityNames() {
    $config_names = [];
    $storage = $this->formState->getStorage();
    foreach ($storage as $data) {
      if (
        $data instanceof ConfigEntityStorageInterface ||
        // Include config entities...
        //
        // @see https://www.drupal.org/project/config_enforce/issues/3251911
        $data instanceof ConfigEntityInterface &&
        // ...if they're not on an entity add/edit form; this prevents config
        // that's technically related, such as the form display mode, from being
        // shown in that context where it's unexpected from a user's point of
        // view and likely only getting in the way of the expected goal of the
        // form: to add or edit content entities. Form display configuration is
        // still shown on the field structure administration pages, where it's
        // expected to be found from the user's point of view.
        !($data instanceof EntityFormDisplayInterface)
      ) {
        $config_names[] = $data->getConfigDependencyName();
      }
    }
    return $config_names;
  }

  /**
   * @return array The list of config names from a ConfigEntityListBuilder form.
   */
  protected function getConfigEntityListNames() {
    $config_names = [];
    $config_entities = $this->formObject->load();
    foreach ($config_entities as $key => $config) {
      $config_names[] = $config->getConfigDependencyName();
    }

    return $config_names;
  }

  /**
   * Determine whether this form contains simple config.
   */
  protected function hasSimpleConfig() {
    return method_exists($this->formObject, 'getEditableConfigNames');
  }

  /**
   * Return the config names from a simple config form.
   */
  protected function getSimpleConfigNames() {
    return $this->invokeProtectedMethod($this->formObject, 'getEditableConfigNames');
  }

  /**
   * Return the form object from the form state.
   */
  protected function determineFormObject() {
    return $this->formState->getBuildInfo()['callback_object'];
  }

  /**
   * Return the form ID from the form state.
   */
  protected function determineFormId() {
    return $this->formState->getBuildInfo()['form_id'];
  }

  /**
   * Invoke a protected method on a given object.
   */
  protected function invokeProtectedMethod($object, $method) {
    if (method_exists($object, $method)) {
      $reflection = new \ReflectionClass(get_class($object));
      $reflection_method = $reflection->getMethod($method);
      $reflection_method->setAccessible(TRUE);
      return $reflection_method->invoke($object);
    }
  }

}
