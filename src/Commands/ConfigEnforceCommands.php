<?php

declare(strict_types=1);

namespace Drupal\config_enforce\Commands;

use Drupal\config_enforce\ConfigEnforcer;
use Drush\Attributes as CLI;
use Drush\Boot\DrupalBootLevels;
use Drush\Commands\DrushCommands;

/**
 * The Drush commandfile for Config Enforce.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ConfigEnforceCommands extends DrushCommands {

  /**
   * Import any configuration that has changed from that on the filesystem.
   */
  #[CLI\Command(name: 'config-enforce:enforce', aliases: ['cee'])]
  #[CLI\Bootstrap(level: DrupalBootLevels::MAX)]
  #[CLI\Option(name: 'only-optional', description: 'Limit enforcement to configs in `config/optional`.')]
  #[CLI\Usage(name: 'drush config-enforce:enforce', description: 'Enforce configuration.')]
  public function enforce($options = ['only-optional' => FALSE]) {
    (new ConfigEnforcer())->enforceConfigs($options['only-optional']);
    $this->logger()->success(dt('Config Enforce enforced configs.'));
  }

}
