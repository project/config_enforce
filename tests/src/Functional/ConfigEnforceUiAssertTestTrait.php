<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\config_enforce\Template\ConfigEnforceAttributesInterface;

/**
 * Provides test assertions for the Config Enforce UI.
 *
 * @todo Add methods to check for enforcement levels.
 */
trait ConfigEnforceUiAssertTestTrait {

  /**
   * Get the Config Enforce container selector.
   *
   * This is the selector used to determine the presence or lack thereof the
   * container element added to a form when:
   *
   * - Config Enforce is enforcing this form's configuration, and/or
   *
   * - Config Enforce Devel is installed, in which case this container will
   *   always be present if Config Enforce identifies it as an enforceable
   *   configuration form.
   *
   * @return string
   */
  protected function getConfigEnforceContainerSelector(): string {
    return '.config-enforce-container';
  }

  /**
   * Get the Config Enforce message that's displayed when a form is enforced.
   *
   * @return string
   *
   * @todo What about translation?
   */
  protected function getConfigEnforceEnforcedMessageText(): string {
    return 'Configuration from this form is being enforced. Any changes may be lost.';
  }

  /**
   * Get the Config Enforce config name data attribute selector.
   *
   * @param string $configName
   *   The configuration machine name to build the selector for.
   *
   * @return string
   *   A CSS selector.
   */
  protected function getConfigEnforceNameDataAttrSelector(
    string $configName
  ): string {
    return '[' . ConfigEnforceAttributesInterface::CONFIG_DATA_ATTR_NAME .
      '="' . $configName .
    '"]';
  }

  /**
   * Get the Config Enforce data attribute selector for enforcement status.
   *
   * @param bool $enforced
   *   If true, will return a selector matching enforced configs; if false, will
   *   return a selector matching unenforced configs.
   *
   * @return string
   *   A CSS selector.
   */
  protected function getConfigEnforceStatusDataAttrSelector(
    bool $enforced
  ): string {
    return '[' . ConfigEnforceAttributesInterface::CONFIG_DATA_ATTR_STATUS .
      '="' . ($enforced === true ? 'true' : 'false') .
    '"]';
  }

  /**
   * Assert that the Config Enforce container exists on the page.
   *
   * @see \Drupal\Tests\WebAssert::elementExists()
   */
  protected function assertConfigEnforceContainerExists(): void {
    $this->assertSession()->elementExists(
      'css', $this->getConfigEnforceContainerSelector()
    );
  }

  /**
   * Assert that the Config Enforce container does not exist on the page.
   *
   * @see \Drupal\Tests\WebAssert::elementNotExists()
   */
  protected function assertConfigEnforceContainerNotExists(): void {
    $this->assertSession()->elementNotExists(
      'css', $this->getConfigEnforceContainerSelector()
    );
  }

  /**
   * Assert that the Config Enforce message is displayed.
   *
   * @see \Drupal\Tests\WebAssert::statusMessageContains()
   */
  protected function assertConfigEnforceStatusMessageExists(): void {
    $this->assertSession()->statusMessageContains(
      $this->getConfigEnforceEnforcedMessageText()
    );
  }

  /**
   * Assert that the Config Enforce message is not displayed.
   *
   * @see \Drupal\Tests\WebAssert::statusMessageNotContains()
   */
  protected function assertConfigEnforceStatusMessageNotExists(): void {
    $this->assertSession()->statusMessageNotContains(
      $this->getConfigEnforceEnforcedMessageText()
    );
  }

  /**
   * Assert that the provided config name appears in the form.
   *
   * @param string $configName
   *   The configuration machine name to check for.
   */
  protected function assertConfigEnforceConfigNameFormExists(
    string $configName
  ): void {
    $this->assertSession()->elementExists('css',
      $this->getConfigEnforceContainerSelector() . ' ' .
      $this->getConfigEnforceNameDataAttrSelector($configName)
    );
  }

  /**
   * Assert that the provided config name does not appear in the form.
   *
   * @param string $configName
   *   The configuration machine name to check for.
   */
  protected function assertConfigEnforceConfigNameFormNotExists(
    string $configName
  ): void {
    $this->assertSession()->elementNotExists('css',
      $this->getConfigEnforceContainerSelector() . ' ' .
      $this->getConfigEnforceNameDataAttrSelector($configName)
    );
  }

  /**
   * Assert that the provided config name appears in the form as enforced.
   *
   * @param string $configName
   *   The configuration machine name to check for.
   */
  protected function assertConfigEnforceConfigNameFormEnforced(
    string $configName
  ): void {
    $this->assertSession()->elementExists('css',
      $this->getConfigEnforceContainerSelector() . ' ' .
      $this->getConfigEnforceNameDataAttrSelector($configName) .
      $this->getConfigEnforceStatusDataAttrSelector(true)
    );
  }

  /**
   * Assert that the provided config name appears in the form as unenforced.
   *
   * @param string $configName
   *   The configuration machine name to check for.
   */
  protected function assertConfigEnforceConfigNameFormNotEnforced(
    string $configName
  ): void {
    $this->assertSession()->elementExists('css',
      $this->getConfigEnforceContainerSelector() . ' ' .
      $this->getConfigEnforceNameDataAttrSelector($configName) .
      $this->getConfigEnforceStatusDataAttrSelector(false)
    );
  }

}
