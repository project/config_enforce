<?php

declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce\ConfigEnforcer;
use Drupal\config_enforce\EnforcedConfigCollection;
use Drupal\config_enforce\Form\EnforceForm;
use Drupal\config_enforce\Form\SettingsForm;
use Drupal\config_enforce\FormHandler\EnforceFormHandler;

/**
 * Implements hook_form_alter().
 */
function config_enforce_form_alter(&$form, FormStateInterface &$form_state, $form_id) {
  // @TODO: Make this a service?
  (new EnforceFormHandler($form, $form_state))->alter();
}

/**
 * Implements hook_config_enforce_form_denylist().
 */
function config_enforce_config_enforce_form_denylist() {
  return [
    EnforceForm::FORM_ID,
    'view_preview_form',
    // @TODO: Skip all confirm forms.
    'block_delete_form',
    // Skip installer forms.
    'install_configure_form',
    'install_select_language_form',
    'install_select_profile_form',
    'install_settings_form',
  ];
}

/**
 * Implements hook_rebuild().
 */
function config_enforce_rebuild() {
  // If we are in the middle of a system update, do not try to synchronize configs.
  if (\Drupal::service('maintenance_mode')->applies(\Drupal::routeMatch())) {
    _log('info', 'Maintenance mode enabled. Skipping config_enforce rebuild.');
    return;
  }

  $configured_triggers = \Drupal::config('config_enforce.settings')->get('triggers');
  $enabled_triggers = is_array($configured_triggers) ? $configured_triggers : SettingsForm::DEFAULT_TRIGGERS;
  if ($enabled_triggers['cache_rebuild']) {
    _log('info', 'Config Enforce Rebuild was triggered.');
    (new ConfigEnforcer())->enforceConfigs();
  }
}

/**
 * Helper method to allow log output within the module.
 */
function _log($level, $message) {
  \Drupal::logger('config_enforce')->$level($message);
}
