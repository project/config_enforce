<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\config_enforce\Functional\ConfigEnforceUiAssertTestTrait;

/**
 * Test that ConfigResolver correctly identifies different config types.
 *
 * @group config_enforce
 */
class ConfigResolverTypesTest extends BrowserTestBase {

  use ConfigEnforceUiAssertTestTrait;

  /**
   * The Drupal module installer service.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected ModuleInstallerInterface $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->moduleInstaller = $this->container->get('module_installer');

  }

  /**
   * Test that we correctly detect configuration on a simple config form.
   */
  public function testSimpleConfigForm(): void {

    /** @var \Drupal\user\UserInterface */
    $adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);

    $this->drupalLogin($adminUser);

    $this->drupalGet('admin/config/system/site-information');

    $this->assertConfigEnforceContainerNotExists();

    $this->moduleInstaller->install([
      'config_enforce_type_simple_config_test',
    ]);

    $this->drupalGet('admin/config/system/site-information');

    $this->assertConfigEnforceContainerExists();

  }

  /**
   * Test that we correctly detect configuration on a config entity form.
   */
  public function testConfigEntityForm(): void {

    $this->moduleInstaller->install(['shortcut']);

    /** @var \Drupal\user\UserInterface */
    $adminUser = $this->drupalCreateUser([
      'administer shortcuts',
    ]);

    $this->drupalLogin($adminUser);

    $this->drupalGet(
      'admin/config/user-interface/shortcut/manage/default/customize'
    );

    $this->assertConfigEnforceContainerNotExists();

    $this->moduleInstaller->install([
      'config_enforce_type_config_entity_test',
    ]);

    $this->drupalGet(
      'admin/config/user-interface/shortcut/manage/default/customize'
    );

    $this->assertConfigEnforceContainerExists();

  }

  /**
   * Test that we correctly detect a configuration entity list builder form.
   *
   * Note that a lot of the classes that extend
   * \Drupal\Core\Config\Entity\ConfigEntityListBuilder in core are not actually
   * forms, which means that we won't have our form alter invoked, so this can
   * only work on one of the lists that's also a form. The block list admin page
   * is one that matches both criteria.
   *
   * Note also that we don't have to place a block ourselves as the block is
   * automatically placed when our test module is installed, because that module
   * provides a block as enforced config; enforcing a block config is also what
   * should result in the Config Enforce container being displayed on this list;
   * if no enforced config is displayed by this list, the Config Enforce
   * container won't be displayed.
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Config%21Entity%21ConfigEntityListBuilder.php/class/hierarchy/ConfigEntityListBuilder
   *   Expanded class hierarchy of ConfigEntityListBuilder listing all core
   *   classes that extend it.
   */
  public function testConfigEntityListBuilderForm(): void {

    $this->moduleInstaller->install([
      'block',
      'config_enforce_entity_list_builder_test',
    ]);

    /** @var \Drupal\user\UserInterface */
    $adminUser = $this->drupalCreateUser([
      'administer blocks',
    ]);

    $this->drupalLogin($adminUser);

    $this->drupalGet('admin/structure/block');

    $this->assertConfigEnforceContainerExists();

    $this->assertConfigEnforceConfigNameFormEnforced(
      'block.block.stark_powered'
    );

  }

  /**
   * Test that we detect configuration on a plug-in-based config entity form.
   *
   * The 'config_enforce_type_plugin_config_test' module contains enforced
   * filter and editor configs; these configs are in the optional direcory, so
   * the correct configs will be automagically installed based on whether the
   * 'ckeditor5' or deprecated 'ckeditor' module is installed due to each being
   * dependent on one module or the other.
   *
   * @todo Remove the ckeditor module usage when we increase the minimum core
   *   version that includes ckeditor5.
   */
  public function testPluginBasedConfigEntityForm(): void {

    /** @var string Either 'ckeditor5' or 'ckeditor' so we know which one is in use. */
    $editor = '';

    // Attempt to install the ckeditor5 module if available. The module
    // installer will throw an exception if it doesn't exist, i.e. on older
    // Drupal core versions.
    try {

      $this->moduleInstaller->install(['ckeditor5']);

      $editor = 'ckeditor5';

    // If that threw an exception, fall back to using the deprecated ckeditor
    // module.
    } catch (\Exception $exception) {

      $this->moduleInstaller->install(['ckeditor']);

      $editor = 'ckeditor';

    }

    $this->assertTrue(
      ($editor === 'ckeditor5' || $editor === 'ckeditor'),
      'Could not install the "CKEditor 5" nor the deprecated "CKEditor" core modules!'
    );

    $this->moduleInstaller->install(['config_enforce_type_plugin_config_test']);

    /** @var \Drupal\user\UserInterface */
    $adminUser = $this->drupalCreateUser([
      'administer filters',
    ]);

    $this->drupalLogin($adminUser);

    $this->drupalGet(
      'admin/config/content/formats/manage/config_enforce_test_format_' .
      $editor
    );

    $this->assertConfigEnforceContainerExists();

    $this->assertConfigEnforceConfigNameFormExists(
      'editor.editor.config_enforce_test_format_' . $editor
    );

  }

}
