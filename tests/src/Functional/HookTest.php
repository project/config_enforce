<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\config_enforce\Functional\ConfigEnforceUiAssertTestTrait;
use Drupal\user\UserInterface;

/**
 * Test that Config Enforce hooks work as intended.
 *
 * @group config_enforce
 */
class HookTest extends BrowserTestBase {

  use ConfigEnforceUiAssertTestTrait;

  /**
   * The admin user used in this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);

  }

  /**
   * Test that \hook_config_enforce_form_denylist() works as intended.
   *
   * Note that this hook only prevents a configuration form being disabled by
   * Config Enforce, and the form can be submitted as usual, but any changes to
   * the configuration made by the user submitting the form will still be
   * ignored due to enforcement of the related configuration. We only test that
   * the form is correctly made editable when the test module containing the
   * hook implementation is installed.
   */
  public function testHookDenyList(): void {

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/config/system/site-information');

    // Check that the site name field is disabled as expected.
    $this->assertSession()->elementAttributeExists(
      'css', 'input[name="site_name"]', 'disabled'
    );

    // Check that the Config Enforce message is displayed.
    $this->assertConfigEnforceStatusMessageExists();

    // Now install the module containing the
    // \hook_config_enforce_form_denylist() implementation that lists this form.
    $this->container->get('module_installer')->install([
      'config_enforce_hook_test',
    ]);

    $this->drupalGet('admin/config/system/site-information');

    // Check that the site name field is no longer disabled.
    $this->assertSession()->elementAttributeNotExists(
      'css', 'input[name="site_name"]', 'disabled'
    );

    // Check that the Config Enforce message is no longer displayed.
    $this->assertConfigEnforceStatusMessageNotExists();

  }

}
