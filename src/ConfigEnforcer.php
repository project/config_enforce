<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\InstallStorage;

/**
 * Enforces configuration.
 */
class ConfigEnforcer {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  const CONFIG_ENFORCE_OFF = 0;
  const CONFIG_ENFORCE_NOSUBMIT = 10;
  const CONFIG_ENFORCE_READONLY = 20;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce\EnforcedConfig';

  /**
   * The enforced config collection.
   *
   * @var \Drupal\config_enforce\EnforcedConfigCollection
   */
  protected $enforcedConfigCollection;

  /**
   * Create a ConfigEnforcer object.
   */
  public function __construct() {
    $this->enforcedConfigCollection = new EnforcedConfigCollection();
  }

  /**
   * Return a list of available enforcement levels.
   */
  public static function getEnforcementLevels() {
    return [
      self::CONFIG_ENFORCE_OFF => t('Allow form & API updates.'),
      self::CONFIG_ENFORCE_NOSUBMIT => t('Allow only API updates.'),
      self::CONFIG_ENFORCE_READONLY => t('Read-only, no updates.'),
    ];
  }

  /**
   * Generate the base64 hash for this config object.
   *
   * @param string $file_path
   *   The path to a config file.
   * @param string $config_name
   *   The name of a config object.
   *
   * @return string
   */
  public function generateHash($file_path, $config_name) {
    if (file_exists($file_path)) {
      return $this->generateHashFromFile($file_path);
    }
    return $this->generateHashFromActiveConfig($config_name);
  }

  /**
   * Re-import read-only config.
   *
   * @param bool $only_optional
   *   Whether to limit enforcement to only those in `config/optional`.
   */
  public function enforceConfigs(bool $only_optional = FALSE) {
    $this->readRegistriesFromDisk();

    $config_files_to_import = [];

    // Since we just re-imported registries' configs, we can safely skip those.
    foreach ($this->getEnforcedConfigCollection()->getEnforcedConfigs(TRUE) as $config_name => $setting) {
      // Only re-import read-only configs.
      if ($setting['enforcement_level'] < self::CONFIG_ENFORCE_READONLY) continue;
      // Skip configs in `config/install` when the "only optional" flag is set.
      if ($only_optional && $setting['config_directory'] != 'config/optional') continue;
      // Only re-import configs that have changed from what's on disk.
      $hash = array_key_exists('hash', $setting) ? $setting['hash'] : '';
      if (!$this->configHasChanged($config_name, $hash)) continue;
      $this->log(self::LOGCHANNEL)
        ->info('Enforced config %name has changed: importing.', ['%name' => $config_name]);
      $config_files_to_import[] = $setting['config_file_path'];
    }

    (new ConfigImporter())->importConfig($config_files_to_import);
  }

  /**
   * Import all enforced config registry configs from disk.
   *
   * We scan the config/install directories of all enabled modules directly,
   * rather than relying on the registries in active config, to find new
   * registries and to ensure we're picking up the canonical values.
   */
  protected function readRegistriesFromDisk() {
    $config_files_to_import = [];

    foreach (\Drupal::moduleHandler()->getModuleDirectories() as $module => $module_path) {
      $config_file_path = $module_path . '/';
      $config_file_path .= InstallStorage::CONFIG_INSTALL_DIRECTORY . '/';
      $config_file_path .= EnforcedConfigRegistry::CONFIG_PREFIX . '.' . $module . '.yml';

      if (!file_exists($config_file_path)) continue;
      $config_files_to_import[] = $config_file_path;
    }

    (new ConfigImporter())->importConfig($config_files_to_import);

    // Flush statically cached enforced configs.
    $this->getEnforcedConfigCollection()->resetEnforcedConfigs();
  }

  /**
   * Return the collection of enforced configs.
   */
  protected function getEnforcedConfigCollection() {
    return $this->enforcedConfigCollection;
  }

  /**
   * Determine whether config in active storage is different from what is on disk.
   *
   * @param string $config_name
   *   The path to a config file.
   * @param string $hash
   *   A base64 hash of the config object.
   *
   * @return bool
   */
  protected function configHasChanged(string $config_name, string $hash) {
    return $hash != $this->generateHashFromActiveConfig($config_name);
  }

  /**
   * Generate base64 hash from a config on disk.
   *
   * @param string $file_path
   *   The path to a config file.
   *
   * @return string
   */
  protected function generateHashFromFile($file_path) {
    $data = file_get_contents($file_path);
    return Crypt::hashBase64($data);
  }

  /**
   * Generate base64 hash from active config object.
   *
   * @param string $config_name
   *   The path to a config file.
   *
   * @return string
   */
  protected function generateHashFromActiveConfig($config_name) {
    $config = \Drupal::config($config_name)->get();
    // @TODO: find a better way to ignore uuid in general case, rather than special case system.site
    if ($config_name != 'system.site') unset($config['uuid']);
    if (array_key_exists('_core', $config)) unset($config['_core']);
    $data = (new InstallStorage())->encode($config);
    return Crypt::hashBase64($data);
  }

}
