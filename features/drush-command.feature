@drush-command
Feature: A Drush command to enforce configs
  In order to reliably import configs from the filesystem
  As a DevOps Engineer
  I need to run a Drush command to enforce configs.

  Background:
    Given I run 'cd ..; make install'

  Scenario: The `config-enforce:enforce` Drush command exists.
    When I run 'drush list'
    Then I should get:
    """
    config-enforce:enforce
    """
    When I run 'drush config-enforce:enforce --help'
    Then I should get:
    """
    Import any configuration that has changed from that on the filesystem.
    Examples:
      config-enforce:enforce Enforce configuration.
    Aliases: cee
    """
    When I run 'drush cee --help'
    Then I should get:
    """
    Import any configuration that has changed from that on the filesystem.
    Examples:
      config-enforce:enforce Enforce configuration.
    Aliases: cee
    """

  Scenario: A target module's optional configs are not imported when it is enabled.
    When I am on the homepage
    Then I should see "Config Enforce Devel"
     And I should not see "Config Enforce -- Test for Drush"
     And I should not see "Drush command appears to work!"
    When I run 'drush pm-enable -y config_enforce_drush_test'
     And I am on the homepage
    Then I should see "Config Enforce Devel"
     And I should not see "Config Enforce -- Test for Drush"
     And I should not see "Drush command appears to work!"

  Scenario: Configs in a module's `config/optional` are imported when our Drush command is run.
    When I run 'drush pm-enable -y config_enforce_drush_test'
     And I run 'drush config-enforce:enforce'
    Then I should get:
     """
     Config Enforce enforced configs.
     """
     And I am on the homepage
    Then I should see "Config Enforce -- Test for Drush"
     And I should see "Drush command appears to work!"
