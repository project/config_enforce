<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\config_enforce\Functional\ConfigEnforceUiAssertTestTrait;
use Drupal\user\UserInterface;

/**
 * Test that the Config enforce form to be at right places.
 *
 * @group config_enforce
 */
class FormConfigDependencyTest extends BrowserTestBase {

  use ConfigEnforceUiAssertTestTrait;

  /**
   * The admin user used in this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce', 'config_enforce_node_test', 'node'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'create page content',
    ]);
  }

  /**
   * Test that config enforce form is not showing on the content forms.
   *
   * Specifically when the related config is enforced.
   *
   * For example, when a content type is enforced, the config enforce
   * form should not show up on the node add/edit form.
   */
  public function testContentForm(): void {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/structure/types/manage/page');

    $this->assertConfigEnforceContainerExists();

    $this->drupalGet('node/add/page');

    // Ensure there is no config enforce form on the node forms.
    $this->assertConfigEnforceContainerNotExists();

  }

}
