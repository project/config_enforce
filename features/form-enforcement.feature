@form-enforcement @api
Feature: Config forms display enforcement behaviours.
  In order to avoid unintentionally breaking the site
  As a Power User
  I need to be warned of enforced configs, and stopped from changing them.

  Background:
    Given I run "cd ..; make dev"

  Scenario: Unenforced config forms are unaffected.
    Given I run 'drush --uri=config-enforce.lndo.site pm-uninstall -y config_enforce_devel'
      And I am logged in as a user with the "administer site configuration" permission
     When I go to "admin/config/system/site-information"
     Then I should not see "Configuration from this form is being enforced. Any changes may be lost."
      And I run 'drush --uri=config-enforce.lndo.site pm-enable -y config_enforce_devel'

# @TODO: Flesh this out with further scenarios
