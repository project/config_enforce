<?php

declare(strict_types=1);

namespace Drupal\config_enforce\FormHandler;

use Drupal\config_enforce\ConfigEnforcer;
use Drupal\config_enforce\EnforcedConfig;
use Drupal\config_enforce\EnforcedConfigCollection;
use Drupal\config_enforce\Form\EnforceForm;
use Drupal\config_enforce\FormHandler\AbstractEnforceFormHandler;
use Drupal\Core\Render\Element;

/**
 * Attach Config Enforce behaviour to config forms.
 */
class EnforceFormHandler extends AbstractEnforceFormHandler {

  const MINIMUM_ENFORCE_LEVEL = ConfigEnforcer::CONFIG_ENFORCE_NOSUBMIT;

  /**
   * Method to call from implementations of hook_alter().
   */
  public function alter() {
    // Only operate on config forms.
    if (!$this->isAnEnforceableForm()) return;

    // Only operate on forms with one or more enforced configs.
    if (!$this->shouldEnforceForm()) return;

    $this->addEnforceForm('Drupal\config_enforce\Form\EnforceForm');

    $this->enforceForm();
  }

  /**
   * Determine whether the Config Enforce Devel module is enabled.
   */
  protected function configEnforceDevelEnabled() {
    return \Drupal::moduleHandler()->moduleExists('config_enforce_devel');
  }

  /**
   * Apply the appropriate level of config enforcement to this form.
   */
  protected function enforceForm() {
    $this->disableFormFields();
    $this->disableFormSubmitHandlers();
  }

  /**
   * Determine whether this form should be enforced.
   *
   * Note that the highest enforcement level, among config supported by this
   * form, will be applied.
   */
  protected function shouldEnforceForm() {
    // Config Enforce Devel overrides this module's behaviour.
    if ($this->configEnforceDevelEnabled()) return FALSE;

    $enforced_configs = (new EnforcedConfigCollection())->getEnforcedConfigs();

    $levels = [];
    foreach ($this->getConfigNames() as $config) {
      if (!array_key_exists($config, $enforced_configs)) continue;
      $levels[$config] = EnforcedConfig::getEnforcementLevel($config);
    }

    // Only display this form if at least one config it provides is being enforced.
    if (empty($levels)) return FALSE;

    return max($levels) >= self::MINIMUM_ENFORCE_LEVEL;
  }

  /**
   * Disable all form fields.
   */
  protected function disableFormFields() {
    foreach ($this->getFields($this->form()) as &$field) {
      $this->disableFormField($field);
    }
  }

  /**
   * Return all fields within a given form element, by reference.
   */
  protected function getFields(&$element) {
    if (!is_array($element)) return [];
    $fields = [];

    foreach (Element::children($element) as $key) {
      $fields[$key] = &$element[$key];
    }

    return $fields;
  }

  /**
   * Disable a given form field recursively.
   */
  protected function disableFormField(&$field) {
    foreach ($this->getFields($field) as &$sub_field) {
      $this->disableFormField($sub_field);
      $sub_field['#attributes']['disabled'] = 'disabled';
      $sub_field['#attributes']['readonly'] = 'readonly';
    }
  }

  /**
   * Disable form submit handlers.
   */
  protected function disableFormSubmitHandlers() {
    $this->form()['#submit'] = [];
  }

}
