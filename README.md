Config Enforce
==============

Config Enforce ensures that specified configuration cannot be changed in
production environments.

For more information see the [documentation
site](https://config-enforce.consensus.enterprises/) or the [project
page](https://www.drupal.org/project/config_enforce)

