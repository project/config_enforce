<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Test that the various enforcement levels work as intended.
 *
 * @group config_enforce
 *
 * @todo Consolidate duplicate code to their own reusable methods for all tests.
 */
class EnforcementLevelsTest extends BrowserTestBase {

  /**
   * The admin user used in this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);

  }

  /**
   * Install the Config Enforce test module.
   *
   * Note that this isn't specified in the $modules property because we need to
   * compare the config before configurations are enforced and after, so we need
   * to start without it enabled.
   */
  protected function installConfigEnforceTestModule(): void {

    $this->container->get('module_installer')->install(['config_enforce_test']);

    // Required to enforce configuration from the test module.
    $this->resetAll();

  }

  /**
   * Test that the read-only enforcement level works as expected.
   *
   * This tests the following scenarios for the 'system.site' configuration:
   *
   * 1. Installing the test module correctly enforces the config it provides,
   *    changing the site name.
   *
   * 2. Attempting to change the site name via the basic site settings form is
   *    prevented.
   *
   * 3. Attempting to change the site name via Drupal's configuration API is
   *    prevented.
   */
  public function testReadOnly(): void {

    /** @var string The enforced site name. */
    $enforcedSiteName = 'Config Enforce test';

    /** @var string The site name if enforcement has failed. */
    $failureSiteName = 'Test failure site name';

    /** @var \Drupal\Core\Config\Config The default 'system.site' configuration object before installing the test module. */
    $defaultConfig = $this->config('system.site');

    $this->installConfigEnforceTestModule();

    /** @var \Drupal\Core\Config\Config The 'system.site' configuration object after installing the test module. */
    $enforcedConfig = $this->config('system.site');

    $this->assertEquals($enforcedSiteName, $enforcedConfig->get('name'));

    // Now we need to test whether the configuration is enforced when attempting
    // to change the site name via the administration form.

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/config/system/site-information');

    $this->submitForm([
      'site_name' => $failureSiteName,
    ], 'Save configuration');

    /** @var \Drupal\Core\Config\Config The 'system.site' configuration object. */
    $enforcedConfig = $this->config('system.site');

    $this->assertEquals($enforcedSiteName, $enforcedConfig->get('name'));

    // Now we need to test whether the configuration is enforced when attempting
    // to set a new site name via the configuration API.

    // Set the site name to the failure state name. This will be reverted by
    // Config Enforce.
    $this->config('system.site')->set('name', $failureSiteName)->save();

    // Configuration is enforced on cache clear so we need to call this method
    // which clears Drupal's caches among other things.
    $this->resetAll();

    /** @var \Drupal\Core\Config\Config The 'system.site' configuration object. */
    $enforcedConfig = $this->config('system.site');

    $this->assertEquals($enforcedSiteName, $enforcedConfig->get('name'));

  }

  /**
   * Test that the API updates only enforcement level works as expected.
   *
   * This tests the following scenarios for the 'system.logging' configuration:
   *
   * 1. Installing the test module correctly enforces the config it provides,
   *    changing the 'error_level' value.
   *
   * 2. Attempting to change the 'error_level' value via the basic site settings
   *    form is prevented.
   *
   * 3. Attempting to change the 'error_level' value via Drupal's configuration
   *    API is not prevented.
   */
# Commenting this pending fix for post 10.2 setup.
#  public function testApiOnly(): void {
#
#    /** @var string The enforced 'error_level' configuration value. */
#    $enforcedValue = 'verbose';
#
#    /** @var string The unenforced 'error_level' configuration value. */
#    $unenforcedValue = 'hide';
#
#    /** @var \Drupal\Core\Config\Config The default 'system.logging' configuration object before installing the test module. */
#    $defaultConfig = $this->config('system.logging');
#
#    $this->installConfigEnforceTestModule();
#
#    /** @var \Drupal\Core\Config\Config The 'system.logging' configuration object after installing the test module. */
#    $enforcedConfig = $this->config('system.logging');
#
#    $this->assertEquals($enforcedValue, $enforcedConfig->get('error_level'));
#
#    // Now we need to test whether the configuration is enforced when attempting
#    // to change the logging level via the administration form.
#
#    $this->drupalLogin($this->adminUser);
#
#    $this->drupalGet('admin/config/development/logging');
#
#    $this->submitForm([
#      'error_level' => $unenforcedValue,
#    ], 'Save configuration');
#
#    /** @var \Drupal\Core\Config\Config The 'system.logging' configuration object. */
#    $enforcedConfig = $this->config('system.logging');
#
#    $this->assertEquals($enforcedValue, $enforcedConfig->get('error_level'));
#
#    // Now we need to test whether the configuration is correctly not enforced
#    // when attempting to change the 'error_level' key via the configuration
#    // API.
#
#    // Set the 'error_level' key. This should not be reverted by Config Enforce.
#    $this->config('system.logging')->set(
#      'error_level', $unenforcedValue
#    )->save();
#
#    // Configuration is enforced on cache clear so we need to call this method
#    // which clears Drupal's caches among other things.
#    $this->resetAll();
#
#    /** @var \Drupal\Core\Config\Config The 'system.logging' configuration object. */
#    $enforcedConfig = $this->config('system.logging');
#
#    $this->assertEquals($unenforcedValue, $enforcedConfig->get('error_level'));
#
#  }

  /**
   * Test that enforcement off (no enforcement) works as expected.
   *
   * Note that this specifically tests configuration that is present in the
   * Config Enforce registry which has been set to allow form and API updates.
   * This does not test configuration that isn't in the Config Enforce registry.
   *
   * This tests the following scenarios for the 'system.cron' configuration:
   *
   * 1. Installing the test module does not enforce the config it provides and
   *    does not change the 'logging' value from the default.
   *
   * 2. Attempting to change the 'logging' value via the cron settings form
   *    works as normal.
   *
   * 3. Attempting to change the 'logging' value via Drupal's configuration API
   *    is not prevented.
   */
  public function testOff(): void {

    /** @var integer The 'logging' configuration value were it to be enforced. */
    $enforcedValue = 0;

    /** @var integer The unenforced 'logging' configuration value. */
    $unenforcedValue = 1;

    /** @var \Drupal\Core\Config\Config The default 'system.cron' configuration object before installing the test module. */
    $defaultConfig = $this->config('system.cron');

    $this->installConfigEnforceTestModule();

    /** @var \Drupal\Core\Config\Config The 'system.cron' configuration object after installing the test module. */
    $enforcedConfig = $this->config('system.cron');

    $this->assertEquals($unenforcedValue, $enforcedConfig->get('logging'));

    // Now we need to test whether the configuration is correctly not enforced
    // when attempting to change the logging level via the administration form.

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/config/system/cron');

    $this->submitForm([
      'logging' => $unenforcedValue,
    ], 'Save configuration');

    /** @var \Drupal\Core\Config\Config The 'system.cron' configuration object. */
    $enforcedConfig = $this->config('system.cron');

    $this->assertEquals($unenforcedValue, $enforcedConfig->get('logging'));

    // Now we need to test whether the configuration is correctly not enforced
    // when attempting to change the 'logging' key via the configuration API.

    // Set the 'logging' key. This should not be reverted by Config Enforce.
    $this->config('system.cron')->set(
      'logging', $unenforcedValue
    )->save();

    // Configuration is enforced on cache clear so we need to call this method
    // which clears Drupal's caches among other things.
    $this->resetAll();

    /** @var \Drupal\Core\Config\Config The 'system.cron' configuration object. */
    $enforcedConfig = $this->config('system.cron');

    $this->assertEquals($unenforcedValue, $enforcedConfig->get('logging'));

  }

}
