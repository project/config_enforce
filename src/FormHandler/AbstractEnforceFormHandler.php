<?php

declare(strict_types=1);

namespace Drupal\config_enforce\FormHandler;

use Drupal\config_enforce\ConfigResolver;
use Drupal\config_enforce\Form\FormHelperTrait;
use Drupal\Core\Form\FormStateInterface;

/**
 * Attach Config Enforce behaviour to config forms.
 */
abstract class AbstractEnforceFormHandler {

  use FormHelperTrait;

  /* A helper object to extract config information from the form. */
  protected $configResolver;

  /**
   * A Simple constructor method.
   */
  public function __construct(&$form, FormStateInterface &$form_state) {
    $this->setSharedFormProperties($form, $form_state);
    $this->configResolver = new ConfigResolver($this->formState());
  }

  /**
   * Method to call from implementations of hook_alter().
   */
  abstract public function alter();

  /**
   * Wrapper for resolver method.
   *
   * @see \Drupal\config_enforce\ConfigResolver::getFormId().
   */
  protected function getFormId() {
    return $this->configResolver->getFormId();
  }

  /**
   * Wrapper for resolver method.
   *
   * @see \Drupal\config_enforce\ConfigResolver::getConfigNames().
   */
  protected function getConfigNames() {
    return $this->configResolver->getConfigNames();
  }

  /**
   * Wrapper for resolver method.
   */
  protected function isAnEnforceableForm() {
    return $this->configResolver->isAnEnforceableForm();
  }

  /**
   * Embed our form in the config form.
   */
  protected function addEnforceForm($class) {
    $config_enforce_context = [
      'form_id' => $this->getFormId(),
      'configs' => $this->getConfigNames(),
    ];

    $config_factory = \Drupal::configFactory();
    $form = new $class($config_factory);
    $this->formState()->set('config_enforce_context', $config_enforce_context);
    $this->form()['config_enforce'] = $form->buildForm([], $this->formState);

    $this->form()['#submit'][] = [$form, 'submitForm'];
  }

}
