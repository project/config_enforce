<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

/**
 * A collection of all enforced configs.
 */
class EnforcedConfigCollection {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce\EnforcedConfigCollection';

  protected $targetModuleCollection;

  /**
   * EnforcedConfigCollection constructor.
   *
   * Build a list of TargetModules with their Registry of Enforced Configs.
   */
  public function __construct() {
    $this->targetModuleCollection = new TargetModuleCollection();
  }

  /**
   * @return \Drupal\config_enforce\TargetModuleCollection
   */
  public function getTargetModuleCollection() {
    return $this->targetModuleCollection;
  }

  /**
   * Return enforcement settings for all enforced configs.
   *
   * @param bool $exclude_registries If TRUE, exclude enforced config registries from the list.
   */
  final public function getEnforcedConfigs($exclude_registries = FALSE) {
    $configs = $this->getAllEnforcedConfigs();
    if (!$exclude_registries) return $configs;
    $registries = $this->getTargetModuleCollection()->getRegistryConfigNames();
    foreach ($configs as $config_name => $setting) {
      if (in_array($config_name, $registries)) {
        unset($configs[$config_name]);
      }
    }
    return $configs;
  }

  /**
   * Get a complete list of enforced config objects.
   *
   * @return array|mixed
   */
  final protected function getAllEnforcedConfigs() {
    $configs = &drupal_static(__METHOD__);
    if (!isset($configs)) {
      $configs = [];
      foreach ($this->getTargetModuleCollection()->getTargetModules() as $target_module) {
        $configs += $target_module->getRegistry()->getEnforcedConfigs();
      }
    }
    return $configs;
  }

  /**
   * Clear the static cache of enforced configs.
   */
  final public function resetEnforcedConfigs() {
    drupal_static_reset('Drupal\config_enforce\EnforcedConfigCollection::getAllEnforcedConfigs');
  }

}
