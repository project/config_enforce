<?php

declare(strict_types=1);

namespace Drupal\config_enforce\Form;

use Drupal\config_enforce\EnforcedConfig;
use Drupal\config_enforce\Form\FormHelperTrait;
use Drupal\config_enforce\Template\ConfigEnforceAttributesInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Attached Config Enforce behaviour for config forms.
 */
abstract class AbstractEnforceForm extends FormBase {

  use FormHelperTrait;

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);
  }

  /**
   * Make our form float above the config form to which it's attached.
   *
   * @deprecated in 1.0.0 and is removed from 2.0.0. No replacement is provided
   *   or needed. Do not use.
   */
  protected function makeFormFloat() {
    @trigger_error(
      'Using this method is deprecated in 1.0.0 and will be removed before 2.0.0.',
      E_USER_DEPRECATED
    );

    $this->form()['#prefix'] = "<div class='config-enforce-form'>";
    $this->form()['#suffix'] = "</div>";
    $this->form()['#attached']['library'][] = 'config_enforce/config-enforce';
    return $this;
  }

  /**
   * Add a details section with information about the config form.
   */
  protected function addConfigFormInfo() {
    $form_id = $this->getContext('form_id');

    $containerClass = 'config-enforce-container';

    $this->form()['config_enforce'] = [
      '#type' => 'details',
      '#tree' =>TRUE,
      '#title' => [
        // This element is used to add the Config Enforce icon.
        '#type'       => 'html_tag',
        '#tag'        => 'span',
        '#attributes' => ['class' => [$containerClass . '__summary-content']],
        '#value'      => $this->t(
          'Config form: %id', ['%id' => $form_id]
        ),
      ],
      '#open' => false,
      '#attributes' => [
        'class' => [$containerClass] + $this->getFormInfoClasses(),
      ],
      '#attached' => ['library' => ['config_enforce/config-enforce']],
    ];

    // Place this as close to the top of the containing form as possible.
    $this->form()['#weight'] = -1000;

    $this->addEnforcedConfigItems();
    return $this;
  }

  /**
   * Determine whether all config objects in this form are enforced.
   */
  protected function allConfigsAreEnforced() {
    foreach ($this->getFormConfigs() as $config_name) {
      if (!EnforcedConfig::isEnforced($config_name)) return FALSE;
    }
    return TRUE;
  }

  /**
   * Return form info classes to allow for better styling.
   */
  protected function getFormInfoClasses(): array {
    if (!$this->allConfigsAreEnforced()) return [];
    $enforcement_levels = [];
    foreach ($this->getFormConfigs() as $config_name) {
      $enforcement_levels[$config_name] = EnforcedConfig::getEnforcementLevel($config_name);
    }
    return [
      'enforced',
      'level-' . min($enforcement_levels),
    ];
  }

  /**
   * Add enforcement settings for each config object managed by this form.
   */
  protected function addEnforcedConfigItems(){
    foreach ($this->getFormConfigs() as $config_name) {
      $this->addEnforcedConfigItem($config_name);
    }
    return $this;
  }

  /**
   * Add enforcement settings for a single config object managed by this form.
   */
  protected function addEnforcedConfigItem($config_name) {
    $this->setCurrentConfig($config_name);

    $config_enforce_enabled = EnforcedConfig::isEnforced($config_name);

    $enforcementLevel = EnforcedConfig::getEnforcementLevel($config_name);

    $this->form()['config_enforce'][$config_name] = [
      '#type' => 'details',
      '#title' => $this->t('Config object: @config', ['@config' => $config_name]),
      '#open' => !EnforcedConfig::isEnforced($config_name),
      '#attributes' => [
        'class' => $this->getRowClasses(),
        // These make it easier to identify which configuration name this item
        // represents in the front-end, whether it's currently enforced, and
        // what the enforcement level is.
        //
        // @see \Drupal\Tests\config_enforce\Functional\ConfigEnforceUiAssertTestTrait
        //   Initial use case is for this to assert whether or not a
        //   configuration item is listed on a given page and/or what its
        //   enforcement status is. Other use cases may be added in the future.
        ConfigEnforceAttributesInterface::CONFIG_DATA_ATTR_NAME =>
          $config_name,
        ConfigEnforceAttributesInterface::CONFIG_DATA_ATTR_STATUS =>
          $config_enforce_enabled ? 'true' : 'false',
        ConfigEnforceAttributesInterface::CONFIG_DATA_ATTR_ENFORCEMENT_LEVEL =>
          $enforcementLevel,
      ],
    ];

    $this->form()['config_enforce'][$config_name]['config_enforce_enabled'] = [
      '#type' => 'item',
      '#title' => $this->t('Config enforce'),
      '#markup' => $config_enforce_enabled ? 'Enabled' : 'Disabled',
    ];

    if ($config_enforce_enabled) {

      $this->form()['config_enforce'][$config_name]['target_module'] = [
        '#type' => 'item',
        '#title' => $this->t('Module'),
        '#markup' => EnforcedConfig::getTargetModule($config_name),
      ];

      $this->form()['config_enforce'][$config_name]['config_directory'] = [
        '#type' => 'item',
        '#title' => $this->t('Config directory'),
        '#markup' => EnforcedConfig::getConfigDirectory($config_name),
      ];

      $this->form()['config_enforce'][$config_name]['config_enforce_path'] = [
        '#type' => 'item',
        '#title' => $this->t('Config file path'),
        '#markup' => EnforcedConfig::getConfigFilePath($config_name),
      ];

      $this->form()['config_enforce'][$config_name]['enforcement_level'] = [
        '#type' => 'item',
        '#title' => $this->t('Enforcement level'),
        '#markup' => EnforcedConfig::getEnforcementLevelLabel($config_name),
      ];

    }
    return $this;
  }

  /**
   * Return an item passed into the form from the config form to which we are attached.
   */
  protected function getContext($key = '') {
    $context = $this->formState()->get('config_enforce_context');
    return empty($key) ? $context : $context[$key];
  }

  /**
   * Return a list of configs embedded in the current form object.
   */
  protected function getFormConfigs() {
    return $this->getContext('configs');
  }

}
