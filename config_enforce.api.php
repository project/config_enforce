<?php

declare(strict_types=1);

/**
 * Documentation for hooks, etc.
 */

/**
 * Return a list of form IDs to skip.
 */
function hook_config_enforce_form_denylist() {
  return [
    'view_preview_form',
  ];
}
