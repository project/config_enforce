<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

/**
 * Enforces configuration.
 */
class EnforcedConfig {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce\EnforcedConfig';

  /* The name of a config object to enforce. */
  protected $configName;

  /* The enforcement level for a config object. */
  protected $enforcementLevel;

  /* The module in which to write a config file. */
  protected $targetModule;

  /* The directory in which to write a config file. */
  protected $configDirectory;

  /* The path to write a config file. */
  protected $configFilePath;

  /* The URI of the host form. */
  protected $configFormUri = '';

  /**
   * Set the config object name attribute.
   */
  public function setConfigName(string $config_name) {
    $this->configName = $config_name;
    return $this;
  }

  /**
   * Set the enforcement level attribute.
   */
  public function setEnforcementLevel(string $level) {
    $this->enforcementLevel = $level;
    return $this;
  }

  /**
   * Set the target module attribute.
   */
  public function setTargetModule(string $module) {
    $this->targetModule = $module;
    return $this;
  }

  /**
   * Set the config directory attribute.
   */
  public function setConfigDirectory(string $directory) {
    $this->configDirectory = $directory;
    return $this;
  }

  /**
   * Set the config directory attribute.
   */
  public function setConfigFormUri(string $uri) {
    $this->configFormUri = $uri;
    return $this;
  }

  /**
   * Return whether a given config object is being enforced.
   */
  public static function isEnforced(string $config_name) {
    return !is_null(self::getEnforcedConfig($config_name));
  }

  /**
   * Return the currently configured target module for a given config object.
   */
  public static function getTargetModule(string $config_name) {
    return self::getEnforcedConfigSetting($config_name, 'target_module');
  }

  /**
   * Return the currently configured config directory for a given config object.
   */
  public static function getConfigDirectory(string $config_name) {
    return self::getEnforcedConfigSetting($config_name, 'config_directory');
  }

  /**
   * Return the currently configured path of the config object.
   */
  public static function getConfigFilePath(string $config_name) {
    return self::getEnforcedConfigSetting($config_name, 'config_file_path');
  }

  /**
   * Return the URI of the host form, if one is available.
   */
  public static function getConfigFormUri(string $config_name) {
    return self::getEnforcedConfigSetting($config_name, 'config_form_uri');
  }

  /**
   * Return a specific setting for a given enforced config.
   */
  public static function getEnforcedConfigSetting(string $config_name, string $key) {
    $settings = self::getEnforcedConfig($config_name);
    if (is_null($settings)) return '';
    return array_key_exists($key, $settings) ? $settings[$key] : '';
  }

  /**
   * Return the currently configured enforcement level for the config object.
   */
  public static function getEnforcementLevel(string $config_name) {
    return self::getEnforcedConfigSetting($config_name, 'enforcement_level');
  }

  /**
   * Return a human readable enforcement level for the config object.
   */
  public static function getEnforcementLevelLabel(string $config_name) {
    return ConfigEnforcer::getEnforcementLevels()[self::getEnforcementLevel($config_name)];
  }

  /**
   * Return this module's configuration for a given config object.
   */
  // @TODO: This doesn't belong in this class.
  public static function getEnforcedConfig(string $config_name) {
    $enforced_configs = (new EnforcedConfigCollection())->getEnforcedConfigs();

    // If a config isn't present, that config is not enforced.
    if (!array_key_exists($config_name, $enforced_configs)) return NULL;

    return $enforced_configs[$config_name];
  }

}
