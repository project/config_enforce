<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

/**
 * A registry of enforced configs for a given target module.
 */
class EnforcedConfigRegistry {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce\EnforcedConfigRegistry';
  const CONFIG_PREFIX = 'config_enforce.registry';

  // The machine name of the target module where this collection of enforced
  // configs is stored.
  protected $targetModule;

  // An instance of the Drupal file system service.
  protected $fileSystem;

  /**
   * A basic constructor method.
   */
  public function __construct(string $target_module) {
    $this->fileSystem = \Drupal::service('file_system');
    $this->setTargetModule($target_module);
  }

  /**
   * Get a list of all config objects under enforcement in this registry.
   *
   * @return array List of enforced configs in this registry, keyed by config object name.
   */
  public function getEnforcedConfigs() {
    $configs = [];
    $target_module_path = \Drupal::service('extension.list.module')->getPath($this->getTargetModule());
    foreach (\Drupal::config($this->getConfigName())->get('enforced_configs') as $encoded_name => $settings) {
      $config_name = self::decode($encoded_name);
      $settings['target_module'] = $this->getTargetModule();
      $settings['config_file_path'] = $this->getDerivedConfigFilePath($target_module_path, $settings['config_directory'], $config_name);
      $configs[$config_name] = $settings;
    }
    return $configs;
  }

  /**
   * Derive the config file path from the target module and config directory.
   *
   * @param string $target_module_path The relative path to the target module.
   * @param string $config_directory The config directory inside the module.
   * @param string $config_name The name of the config object.
   *
   * @return string The complete path to the enforced config object on disk.
   */
  public function getDerivedConfigFilePath(string $target_module_path, string $config_directory, string $config_name) {
    $config_file_path  = $target_module_path . DIRECTORY_SEPARATOR;
    $config_file_path .= $config_directory . DIRECTORY_SEPARATOR;
    $config_file_path .= $config_name . '.yml';
    return $config_file_path;
  }

  /**
   * Consistently encode config names, so that they can be used as array keys when saved.
   *
   * Config keys cannot use `.`, whereas config names cannot use `:`. So this
   * encoding should avoid any possible overlap with config file and naming
   * methods.
   *
   * Related issue: https://www.drupal.org/project/config_enforce/issues/3173621.
   *
   * @see ConfigBase::validateKeys().
   * @see ConfigBase::validateName().
   * @see decode().
   */
  // @TODO: This doesn't belong in this class.
  public static function encode($config_name) {
    return str_replace('.', ':', $config_name);
  }

  /**
   * Consistently decode config names.
   *
   * @see encode().
   */
  // @TODO: This doesn't belong in this class.
  public static function decode($encoded_config_name) {
    return str_replace(':', '.', $encoded_config_name);
  }

  /**
   * Set the target module for this enforced config.
   */
  public function setTargetModule(string $target_module) {
    $this->targetModule = $target_module;
    return $this;
  }

  /**
   * Return the target module for this enforced config.
   */
  public function getTargetModule() {
    return $this->targetModule;
  }

  /**
   * Return the config name for the target module.
   */
  public function getConfigName() {
    return self::CONFIG_PREFIX . '.' . $this->getTargetModule();
  }

}
