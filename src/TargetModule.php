<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

/**
 * A module that contains enforced configs.
 */
class TargetModule {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce\TargetModule';

  // A registry of enforced configs.
  protected $registry;

  // The module machine.
  protected $machineName;

  /**
   * TargetModule constructor.
   *
   * @param $machine_name
   */
  public function __construct($machine_name) {
    $this->registry = new EnforcedConfigRegistry($machine_name);
    $this->machineName = $machine_name;
  }

  /**
   * @return \Drupal\config_enforce\EnforcedConfigRegistry
   */
  public function getRegistry() {
    return $this->registry;
  }

  /**
   * Return the module machine name.
   */
  public function getMachineName() {
    return $this->machineName;
  }

}
