<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\FunctionalJavascript;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\UserInterface;

/**
 * Test that Ajax sub-forms work correctly with Config Enforce installed.
 *
 * @group config_enforce
 *
 * @see https://www.drupal.org/project/config_enforce/issues/3315807
 *   Intended to catch regressions of this issue.
 */
class AjaxSubFormCompatibilityTest extends WebDriverTestBase {

  /**
   * Field name created to test Ajax functionality.
   */
  protected const FIELD_NAME = 'field_ajax_test';

  /**
   * The user entity created for this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['field_ui', 'node', 'config_enforce'];

  /**
   * {@inheritdoc}
   *
   * This does the following to set up the tests:
   *
   * - Creates the 'page' content type.
   *
   * - Creates the test field via the field storage.
   *
   * - Attaches the test field to the 'page' content type.
   *
   * - Sets the default form and view display modes.
   *
   * - Creates a user with the necessary permissions to edit node displays, and
   *   logs in this new user.
   */
  protected function setUp(): void {

    parent::setUp();

    $this->entityDisplayRepository = $this->container->get(
      'entity_display.repository'
    );

    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    /** @var \Drupal\field\FieldStorageConfigInterface */
    $fieldStorage = FieldStorageConfig::create([
      'field_name'  => self::FIELD_NAME,
      'entity_type' => 'node',
      'type'        => 'text_long',
      'cardinality' => 1,
    ]);

    $fieldStorage->save();

    FieldConfig::create([
      'field_storage' => $fieldStorage,
      'field_name'    => self::FIELD_NAME,
      'entity_type'   => 'node',
      'bundle'        => 'page',
    ])->save();

    $this->entityDisplayRepository->getFormDisplay('node', 'page', 'default')
      ->setComponent(self::FIELD_NAME, [
        'type'      => 'text_textarea',
        'settings'  => ['rows' => 5, 'placeholder' => ''],
      ])
      ->save();

    $this->entityDisplayRepository->getViewDisplay('node', 'page', 'default')
      ->setComponent(self::FIELD_NAME, ['type' => 'text_default'])
      ->save();

    $this->testUser = $this->drupalCreateUser([
      'access administration pages',
      'administer node fields',
      'administer node display',
      'administer node form display',
    ]);

    $this->drupalLogin($this->testUser);

  }

  /**
   * Test that node form display Ajax sub-forms work as expected.
   */
  public function testFormDisplaySubForm(): void {

    $this->drupalGet('admin/structure/types/manage/page/form-display');

    $this->assertSession()->buttonExists(self::FIELD_NAME . '_settings_edit');

    /** @var \Drupal\Tests\DocumentElement */
    $page = $this->getSession()->getPage();

    $page->pressButton(self::FIELD_NAME . '_settings_edit');

    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertSession()->fieldExists(
      'fields[' . self::FIELD_NAME . '][settings_edit_form][settings][rows]'
    );

  }

  /**
   * Test that node view display Ajax sub-forms work as expected.
   *
   * This changes the test field display format from "Default" to "Trimmed",
   * verifies that the gear icon button has been inserted via Ajax, clicks the
   * button, and finally verifies that the "Trimmed limit" field exists.
   */
  public function testViewDisplaySubForm(): void {

    $this->drupalGet('admin/structure/types/manage/page/display');

    $selectName = 'fields[' . self::FIELD_NAME . '][type]';

    $this->assertSession()->selectExists($selectName);

    /** @var \Drupal\Tests\DocumentElement */
    $page = $this->getSession()->getPage();

    $this->assertSession()->fieldValueEquals($selectName, 'text_default');

    $page->selectFieldOption($selectName, 'text_trimmed');

    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertSession()->buttonExists(self::FIELD_NAME . '_settings_edit');

    $page->pressButton(self::FIELD_NAME . '_settings_edit');

    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->assertSession()->fieldExists('fields[' .
      self::FIELD_NAME .
    '][settings_edit_form][settings][trim_length]');

  }

}
