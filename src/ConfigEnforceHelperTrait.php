<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

trait ConfigEnforceHelperTrait {
  use StringTranslationTrait, MessengerTrait, LoggerChannelTrait;

  protected function log($channel = 'config_enforce') {
    return $this->getLogger($channel);
  }

  protected function configFactory() {
    return \Drupal::configFactory();
  }
}
