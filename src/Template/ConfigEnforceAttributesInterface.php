<?php

declare(strict_types=1);

namespace Drupal\config_enforce\Template;

/**
 * Interface defining various Config Enforce element attribute names.
 */
interface ConfigEnforceAttributesInterface {

  /**
   * Name of data attribute added to each config item containing its name.
   *
   * @see \Drupal\config_enforce\Form\AbstractEnforceForm::addEnforcedConfigItem()
   *   Output in this method.
   */
  public const CONFIG_DATA_ATTR_NAME = 'data-config-enforce-config-name';

  /**
   * Data attribute added to each config item containing its enforcement status.
   *
   * @see \Drupal\config_enforce\Form\AbstractEnforceForm::addEnforcedConfigItem()
   *   Output in this method.
   */
  public const CONFIG_DATA_ATTR_STATUS = 'data-config-enforce-config-status';

  /**
   * Data attribute added to each config item containing its enforcement level.
   *
   * @see \Drupal\config_enforce\Form\AbstractEnforceForm::addEnforcedConfigItem()
   *   Output in this method.
   */
  public const CONFIG_DATA_ATTR_ENFORCEMENT_LEVEL =
    'data-config-enforce-config-enforce-level';

}
