<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test that the module installs succesfully.
 *
 * @group config_enforce
 */
class ModuleInstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce'];

  /**
   * If the module installs, that means its schema is valid.
   */
  public function testModuleInstallSuccessful() {
    $this->assertTrue(TRUE);
  }

}
