<?php

declare(strict_types=1);

namespace Drupal\config_enforce\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\config_enforce\EnforcedConfig;

/**
 * Methods to simplify form handling.
 */
trait FormHelperTrait {

  use StringTranslationTrait, MessengerTrait;

  /* The form array. */
  private $form;

  /* The form state object. */
  private $formState;

  /* The current config object name. */
  protected $current_config;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return get_called_class()::FORM_ID;
  }

  /**
   * Set common form properties on the object, for easier OOP.
   */
  protected function setSharedFormProperties(array &$form, FormStateInterface &$form_state) {
    $this->form =& $form;
    $this->formState =& $form_state;
    return $this;
  }

  /**
   * Return the form array.
   */
  protected function &form() {
    return $this->form;
  }

  /**
   * Return the form state object.
   */
  protected function &formState() {
    return $this->formState;
  }

  /**
   * Add a warning to the top of this form.
   */
  protected function addWarning($message, $tokens) {

    $this->form()['warning'] = [
      '#theme'        => 'status_messages',
      '#message_list' => [
        'warning' => [
          $this->t($message, $tokens),
        ],
      ],
      '#status_headings' => [
        'warning' => $this->t('Warning message'),
      ],
      '#weight' => -10,
    ];

    return $this;

  }

  /**
   * Returns an HTML-formatted list.
   */
  protected function renderHtmlList($items) {
    $list = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return \Drupal::service('renderer')->render($list);
  }

  /**
   * Return row classes to allow for better styling.
   */
  protected function getRowClasses() {
    $config_name = $this->getCurrentConfig();
    $classes = [];
    if (EnforcedConfig::isEnforced($config_name)) {
      $classes[] = 'enforced';
      $classes[] = 'level-' . EnforcedConfig::getEnforcementLevel($config_name);
    }
    return $classes;
  }

  /**
   * Set the current config object name.
   */
  protected function setCurrentConfig($config) {
    $this->current_config = $config;
    return $this;
  }

  /**
   * Return the current config object name.
   */
  protected function getCurrentConfig() {
    return $this->current_config;
  }

}
