<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\config_enforce\Functional\ConfigEnforceUiAssertTestTrait;
use Drupal\user\UserInterface;

/**
 * Test that Config Enforce data attributes are output as expected.
 *
 * @group config_enforce
 *
 * @todo Expand this to include enforcement level data attributes.
 */
class DataAttributesTest extends BrowserTestBase {

  use ConfigEnforceUiAssertTestTrait;

  /**
   * The admin user used in this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);

  }

  /**
   * Install the Config Enforce test module.
   *
   * Note that this isn't specified in the $modules property because we need to
   * compare the config before configurations are enforced and after, so we need
   * to start without it enabled.
   */
  protected function installConfigEnforceTestModule(): void {

    $this->container->get('module_installer')->install(['config_enforce_test']);

  }

  /**
   * Test that data attributes are being output correctly.
   *
   * First we check that the expected attribute format is created by the test
   * trait, and then we actually log in and verify that the data attributes are
   * found when enforcing that form's configuration. These checks are done both
   * with the hard-coded CSS selectors and then also using the test trait's
   * assert methods.
   */
  public function testDataAttributes(): void {

    $this->assertTrue((
      $this->getConfigEnforceNameDataAttrSelector('system.site') ===
      '[data-config-enforce-config-name="system.site"]'
    ), \sprintf(
      'Data attribute selector for "%s" is "%s" but expected to be "%s".',
      'system.site',
      $this->getConfigEnforceNameDataAttrSelector('system.site'),
      '[data-config-enforce-config-name="system.site"]'
    ));

    $this->assertTrue((
      $this->getConfigEnforceStatusDataAttrSelector(true) ===
      '[data-config-enforce-config-status="true"]'
    ), \sprintf(
      'Data attribute selector is "%s" but expected to be "%s".',
      $this->getConfigEnforceStatusDataAttrSelector(true),
      '[data-config-enforce-config-status="true"]'
    ));

    $this->assertTrue((
      $this->getConfigEnforceStatusDataAttrSelector(false) ===
      '[data-config-enforce-config-status="false"]'
    ), \sprintf(
      'Data attribute selector is "%s" but expected to be "%s".',
      $this->getConfigEnforceStatusDataAttrSelector(false),
      '[data-config-enforce-config-status="false"]'
    ));

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/config/system/site-information');

    // This configuration should not be listed because it's not enforced yet.
    $this->assertSession()->elementNotExists('css',
      '.config-enforce-container [data-config-enforce-config-name="system.site"]'
    );

    $this->assertConfigEnforceConfigNameFormNotExists('system.site');

    $this->installConfigEnforceTestModule();

    $this->drupalGet('admin/config/system/site-information');

    // Now this is enforced, so we expect it to exist.
    $this->assertSession()->elementExists('css',
      '.config-enforce-container [data-config-enforce-config-name="system.site"]'
    );

    $this->assertConfigEnforceConfigNameFormExists('system.site');

    // This configuration is not enforced on this form, so we don't expect it to
    // be listed here.
    $this->assertConfigEnforceConfigNameFormNotExists('system.logging');

  }

}
