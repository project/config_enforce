<?php

declare(strict_types=1);

namespace Drupal\config_enforce;

/**
 * A collection of all target modules.
 */
class TargetModuleCollection {

  // Use log() method and related traits.
  use ConfigEnforceHelperTrait;

  // Channel with which to log from this class.
  const LOGCHANNEL = 'config_enforce\TargetModuleCollection';

  /**
   * A basic constructor method.
   */
  public function __construct() {}


  /**
   * Return a list of all enforced config registries.
   */
  public function getRegistryConfigNames() {
    $registry_config_names = &drupal_static(__METHOD__);
    if (!isset($registry_config_names)) {
      $registry_config_names = $this->configFactory()->listAll(EnforcedConfigRegistry::CONFIG_PREFIX);
    }
    return $registry_config_names;
  }

  /**
   * Return a list of modules and their labels.
   */
  public function getAllModules() {
    $modules = &drupal_static(__METHOD__);
    if (!isset($modules)) {
      $moduleHandler = \Drupal::moduleHandler();
      foreach ($moduleHandler->getModuleList() as $name => $module) {
        $modules[$name] = $moduleHandler->getName($name);
      }
      asort($modules);
    }
    return $modules;
  }

  /**
   * Return human-readable names for all target modules.
   */
  public function getTargetModuleLabels() {
    $modules = [];
    $available_modules = $this->getTargetModuleNames();
    $available_modules = !is_null($available_modules) ? $available_modules : [];
    foreach ($available_modules as $module) {
      $modules[$module] = $this->getAllModules()[$module];
    }
    return $modules;
  }

  /**
   * Return a human-readable names for a target module.
   */
  public function getTargetModuleLabel(string $module) {
    return $this->getTargetModuleLabels()[$module];
  }

  /**
   * Instantiate a TargetModule object.
   */
  protected function newTargetModule($module) {
    return new TargetModule($module);
  }

  /**
   * Return a list of target module names.
   */
  public function getTargetModuleNames() {
    $target_module_names = [];
    foreach ($this->getRegistryConfigNames() as $registry_name) {
      $target_module_names[] = substr($registry_name, strlen(EnforcedConfigRegistry::CONFIG_PREFIX) + 1);
    }
    return $target_module_names;
  }

  /**
   * Return a list of available modules to target for writing config.
   */
  public function getTargetModules() {
    $target_modules = &drupal_static(__METHOD__);
    if (!isset($target_modules)) {
      $target_modules = [];
      foreach ($this->getTargetModuleNames() as $module) {
        $target_modules[$module] = $this->newTargetModule($module);
      }
    }
    return $target_modules;
  }

}
