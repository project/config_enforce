@cache-rebuild
Feature: Optionally enforce configs on cache rebuild.
  In order to easily import configs from the filesystem
  As a Developer
  I want a cache rebuild to trigger config enforcement.

  Background:
    Given I run 'cd ..; make install'

  Scenario: A target module's optional configs are not imported when it is enabled.
    When I am on the homepage
    Then I should see "Config Enforce Devel"
     And I should not see "Config Enforce -- Test for Drush"
     And I should not see "Drush command appears to work!"
    When I run 'drush pm-enable -y config_enforce_drush_test'
     And I am on the homepage
    Then I should see "Config Enforce Devel"
     And I should not see "Config Enforce -- Test for Drush"
     And I should not see "Drush command appears to work!"

  Scenario: Configs in a module's `config/optional` are imported when caches are rebuilt, by default.
   Given I run 'drush pm-enable -y config_enforce_drush_test'
     And I run 'drush cache-rebuild --verbose'
    When I run 'drush watchdog-show'
    Then I should get:
     """
     Config Enforce Rebuild was triggered.
     """
     And I am on the homepage
    Then I should see "Config Enforce -- Test for Drush"
     And I should see "Drush command appears to work!"

  @api
  Scenario: Triggering enforcement on cache clear can be disabled.
   Given I am logged in as an "Admin"
    When I am on "/admin/config/development/config_enforce"
     And I uncheck "edit-triggers-cache-rebuild"
     And I press "Save configuration"
    Then I should see "The configuration options have been saved."
    When I run 'drush pm-enable -y config_enforce_drush_test'
     And I run 'drush cache-rebuild --verbose'
    Then I should not get:
     """
     [info] Config Enforce Rebuild was triggered.
     """
     And I am on the homepage
    Then I should not see "Config Enforce -- Test for Drush"
     And I should not see "Drush command appears to work!"
    When I run 'drush config-enforce:enforce --verbose'
    Then I should get:
     """
     [success] Config Enforce enforced configs.
     """
     And I am on the homepage
    Then I should see "Config Enforce -- Test for Drush"
     And I should see "Drush command appears to work!"
